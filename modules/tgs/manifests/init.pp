# Class: nds_tgs
#
# This class installs tgs
#
# Parameters:
#
# Actions:
#   - Installs RPM version of tokenGeneratorServer
#
class nds_tgs () {
  
  $tgs_version = hiera('tgs::version')
  $rpm_package = "nds_drms_tgs-${tgs_version}_el5_11g"
  $base_dir = "/opt/nds/tokenGeneratorServer"
  $source_url = hiera('bin_repo::url')
  $config_props = hiera('tgs::config_props', [])
  $java_home = hiera('java::home', "/usr/java/jdk1.7.0_05")
  
  package { "$rpm_package":
    provider => 'yum',
    ensure   => installed,
  } ->
  
  admin::augeas_prop_transform { "${base_dir}/etc/config.properties":
       source => "${base_dir}/docs/sample/config.properties",
       changes => [    
			 $config_props   
        ],
        require => Package["$rpm_package"]
#        notify  => Service['nds_tokenGeneratorServer'],
  } ->
  file { "${base_dir}/etc/generatedFiles":
    ensure => directory,
    owner   => ndsuser,
    group   => nds,
    mode    => 644,
  } ->
  file { "${base_dir}/etc/nds_tokenGeneratorServer.cfg":
  		source => "${base_dir}/docs/sample/nds_tokenGeneratorServer.cfg",
      owner   => ndsuser,
      group   => nds,
      mode    => 644,
  } ->
  file { "${base_dir}/etc/log4j.properties":
        source  => "${base_dir}/docs/sample/log4j.properties",
        owner   => ndsuser,
        group   => nds,
        mode    => 644,
  } #->
#  file { "${base_dir}/etc/generatedFiles/CookieKeys.dat":
#      source => "puppet:///modules/nds_tgs/CookieKeys.dat",
#      owner   => "ndsuser",
#      group   => "nds",
#      mode    => 600,
#  } ->
#
#  file { "$base_dir/bin/java_tokenGeneratorServer":
#        ensure  => link,
#        target  => "${java_home}/bin/java",
#        owner   => ndsuser,
#        group   => nds,
#  } ->
#  service { "nds_tokenGeneratorServer":
#            ensure => running,
#            enable => true,
#  }
}
