class nds_authz () {

  $sgw_version = hiera('sgw::version')
	package { "${rpm_package}":
		provider => 'yum',
		ensure => 'installed',
	} 



	admin::augeas_prop_transform { "${base_dir}/etc/log4j.properties":
		source => "${base_dir}/docs/sample/log4j.properties",
		changes => ["set log4j.logger.com.nds ${log_level}",
		],
		require => [Package["$rpm_package"],
		],
	}

	file { "${base_dir}/log":
		ensure => 'link',
		target => '/var/log/nds/authz',
		owner => 'ndsuser',
		group => 'nds',
		mode => 644,
		require => [Package["$rpm_package"],
		],
	} 


	file { "$base_dir/etc/config.properties":
		owner => "ndsuser",
		group => "nds",
		mode => 644,
		content => template('nds_authz/config.properties.erb'),
		before => Service["nds_authz"],
		require => [Package["$rpm_package"],
		],
	} 

	file { "$base_dir/etc/":
		source => "$base_dir/docs/sample/",
		owner => "ndsuser",
		group => "nds",
		mode => 644,
		recurse => true,
		ignore => ["config.properties",
		"log4j.properties",
		"[a-zA-Z]*json",
		]
	}

}
