module Puppet::Parser::Functions
      newfunction(:set_from_hiera, :type => :rvalue) do |args|
        Puppet::Parser::Functions.autoloader.loadall
        params = args[0]
        set_param_list = []
        params.each_pair do |key, value|
          debug "Parameter #{key}, value #{value}"
          hiera_value = function_hiera([value,])
          set_param_list.push("set #{key} #{hiera_value}")
          debug "set #{key} #{hiera_value}"
        end
        return set_param_list
      end
end
