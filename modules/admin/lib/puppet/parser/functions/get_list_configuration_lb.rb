module Puppet::Parser::Functions
  newfunction(:get_list_configuration_lb, :type => :rvalue) do |args|
    variable = args[0]
    info("********************** get_list_configuration_lb **********************")
    #variable = lookupvar("#{var_name}")
    if variable.instance_of? String
      return variable.split(";;")
    else
      return variable
    end
  end
end