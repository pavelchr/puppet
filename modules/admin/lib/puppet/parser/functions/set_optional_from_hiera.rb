module Puppet::Parser::Functions
      newfunction(:set_optional_from_hiera, :type => :rvalue) do |args|
        debug "set_if_provided called"
        Puppet::Parser::Functions.autoloader.loadall
        params = args[0]
        debug "Parameter dict: #{params}"
        set_param_list = []
        params.each_pair do |key, value|
          debug "Parameter #{key}, value #{value}"
          hiera_value = function_hiera([value, ''])
          if hiera_value != ''
            set_param_list.push("set #{key} #{hiera_value}")
            debug "set #{key} #{hiera_value}"
          end
        end
        return set_param_list
      end
end
